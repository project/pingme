<?php

namespace Drupal\pingme\Tests;

use Drupal\simpletest\WebTestBase;

/**
 * Provides automated tests for the pingme module.
 */
class ViewSingleRecordControllerTest extends WebTestBase {


  /**
   * {@inheritdoc}
   */
  public static function getInfo() {
    return [
      'name' => "pingme ViewSingleRecordController's controller functionality",
      'description' => 'Test Unit for module pingme and controller ViewSingleRecordController.',
      'group' => 'Other',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function setUp() {
    parent::setUp();
  }

  /**
   * Tests pingme functionality.
   */
  public function testViewSingleRecordController() {
    // Check that the basic functions of module pingme.
    $this->assertEquals(TRUE, TRUE, 'Test Unit Generated via Drupal Console.');
  }

}
