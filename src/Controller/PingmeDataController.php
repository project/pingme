<?php

namespace Drupal\pingme\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\Core\Link;
use Drupal\Core\Asset\LibraryDiscoveryParser;
use Drupal\Core\Database\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Component\Serialization\Json;

/**
 * Class PingmeDataController.
 */
class PingmeDataController extends ControllerBase {

  /**
   * Pingmedata.
   *
   * @return string
   *   Return Hello string.
   */
  public function PingMeData() 
  {
    $form['open_modal'] = [
      '#type' => 'link',
      '#title' => $this->t('New Message'),
      '#url' => Url::fromRoute('pingme.modal_form_example_controller_openModalForm'),
      '#attributes' => [
        'class' => [
          'use-ajax',
          'button',
        ],
      ],
    ];
    $form['text_footer'] = array
    (
      '#prefix' => '<hr>',
      '#suffix' => '</hr>',
    );
    $query    = \Drupal::database()->select('pingme', 'pm');
    $query->fields('pm', ['id','reciever','reciever_name','message','reciever_email']);
    $query->condition('isDeleted',0);
    $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
    $results  = $pager->execute()->fetchAll();
    $data = [];
    $ModalForm = \Drupal::formBuilder()->getForm('Drupal\pingme\Form\ChatForm');

    $header = [
      'id'        => t('ID'),
      'name'      => t('Name'),
      'email'     => t('E-mail'),
      'message'   => t('Message'),
      'view'      => t('View'),
      'edit'      => t('Edit'),
      'delete'    => t('Delete'),
    ];
    if($results != '' && !empty($results))
    {
      foreach($results as $row => $res)
      {
        // $edit_lnk            = Url::fromRoute('pingme.chat_form.edit',['id'=>$res->id],[]);
        // $delete_lnk          = Url::fromRoute('pingme.delete_messages_form',['id'=>$res->id],[]);
        // $edit_link           = Link::fromTextAndUrl(t('Edit'),$edit_lnk);
        // $delete_link         = Link::fromTextAndUrl(t('Delete'),$delete_lnk);
        // $edit_link           = $edit_link->toRenderable();
        // $delete_link         = $delete_link->toRenderable();
  
        // $mainLink = t('@linkApprove  @linkReject', array('@linkApprove' => $edit_link, '@linkReject' => $delete_link));
        
        $edit_link = Url::fromRoute('pingme.chat_form.edit',['id'=>$res->id],[]); 
        $edit_link->setOptions([
          'attributes' => [
            'class' => ['use-ajax', 'button', 'button--small'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode(['width' => 800]),
          ]
        ]);
        $delete_lnk          = Url::fromRoute('pingme.delete_messages_form',['id'=>$res->id],[]);
        $delete_lnk->setOptions([
          'attributes' => [
            'class' => ['use-ajax', 'button', 'button--small'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode(['width' => 800]),
          ]
        ]);

        $view_link          = Url::fromRoute('pingme.view_single_record_controller_ViewrRecord',['id'=>$res->id],[]);
        $view_link->setOptions([
          'attributes' => [
            'class' => ['use-ajax', 'button', 'button--small'],
            'data-dialog-type' => 'modal',
            'data-dialog-options' => Json::encode(['width' => 800]),
          ]
        ]);

        $data[$res->id] = [
          'id'        => $res->id,
          'name'      => $res->reciever_name,
          'email'     => $res->reciever_email,
          'message'   => $res->message,
          'view'      => Link::fromTextAndUrl(t('View'), $view_link)->toString(),
          'edit'      => Link::fromTextAndUrl(t('Edit'), $edit_link)->toString(),
          'delete'    => Link::fromTextAndUrl(t('Delete'), $delete_lnk)->toString(),
        ];
      }
    }
    else
    {
      $data = [];
    }
    

    $response = new AjaxResponse();
    $modal_form = \Drupal::formBuilder()->getForm('Drupal\pingme\Form\ChatForm');
    $response->addCommand(new OpenModalDialogCommand('Chat Form', $modal_form, ['width' => '800']));

    $form['table'] = [
      '#type'       => 'table',
      '#header'     => $header,
      '#rows'       => $data,
      '#attributes' => [
                      'class' => [
                        'table',
                        'table-striped',
                        'table-responsive',
                        'table-dark'
                      ],
                      'border' => '1px'
      ],
    ];
    $form['pager'] = [
      '#type' => 'pager',
    ];
    $form['#attached']['library'] = [
      'core/drupal.dialog.ajax',
      'pingme/pingme.tree'
    ];
    return $form;
  }

}
