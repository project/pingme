<?php

namespace Drupal\pingme\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\Element\EntityAutocomplete;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Drupal\Component\Utility\Xss;

/**
 * Class LoadUsersController.
 */
class LoadUsersController extends ControllerBase {

  /**
   * Loadusers.
   *
   * @return string
   *   Return Hello string.
   */
  public function LoadUsers(Request $request) {
    $results = [];
    $input = $request->query->get('q');
    if (!$input) 
    {
      return new JsonResponse($results);
    }
    $input = Xss::filter($input);
    $database = \Drupal::database();
    $result = $database->select('users_field_data', 'u')
              ->fields('u')
              ->condition('name', "%" . $database->escapeLike($input) . "%", 'LIKE')
              ->execute()
              ->fetchAll();
    foreach ($result as $user) 
    {
      $records[] = [
        'value' => $user->uid,
        'label' => $user->name.'('.$user->uid.')',
      ];
    }

    return new JsonResponse($records);
  }

}
