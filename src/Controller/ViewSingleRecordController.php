<?php

namespace Drupal\pingme\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Url;
use Drupal\Core\Routing;
use Drupal\Core\Link;
use Drupal\Core\Asset\LibraryDiscoveryParser;
use Drupal\Core\Database\Database;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Component\Serialization\Json;

/**
 * Class ViewSingleRecordController.
 */
class ViewSingleRecordController extends ControllerBase {

  protected $id;
  /**
   * Viewrrecord.
   *
   * @return string
   *   Return Hello string.
   */
  public function ViewrRecord($id = NULL) {
    $head = [];
    $row  = [];
    if(!empty($id))
    {
      $query    = \Drupal::database()->select('pingme', 'pm');
      $query->fields('pm', ['id','reciever','reciever_name','message','reciever_email']);
      $query->condition('id',$id);
      $results  = $query->execute()->fetchAll();

      $head = [
        'name'      => t('Name'),
        'email'     => t('E-mail'),
        'message'   => t('Message'),
      ];

      if($results != '' && !empty($results))
      {
        foreach($results as $res)
        {
          $row[$res->id] = [
            'name'      => $res->reciever_name,
            'email'     => $res->reciever_email,
            'message'   => $res->message,
          ];
          
        }
      }

      $form['table'] = [
        '#type'   => 'table',
        '#header' => $head,
        '#rows'   => $row,   
      ];
    }
    $form['#attached']['library'] = [
      'core/drupal.dialog.ajax',
      'pingme/pingme.tree'
    ];
    return $form;
  }

}
