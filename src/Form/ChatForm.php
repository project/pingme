<?php

namespace Drupal\pingme\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\user\Entity\User;
use \Drupal\Core\Database\Connection;
use Drupal\Core\Ajax\AjaxResponse;
use Drupal\Core\Ajax\OpenModalDialogCommand;
use Drupal\Core\Ajax\ReplaceCommand;
use \Drupal\Core\Routing;

/**
 * Class ChatForm.
 */
class ChatForm extends FormBase {

  protected $id;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'chat_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {

    $form['#prefix']    = '<div id="modal_example_form">';
    $form['#suffix']    = '</div>';
    $form['id']         = [
      '#type'           => 'hidden',
      '#default_value'  => $id != '' ? t($id) : '',
    ];

    // Static Variables
    $message = ''; $param   =  $id; $name    = ''; $uid     = '';
    
    // IF REQUESTED ID HAVING VALUE

    if(!empty($param))
    {
      $query    = \Drupal::database()->select('pingme', 'pm');
      $query->fields('pm', ['id','reciever','reciever_name','message','reciever_email']);
      $query->condition('isDeleted',0);
      $query->condition('id',$param);
      $pager = $query->extend('Drupal\Core\Database\Query\PagerSelectExtender')->limit(10);
      $results  = $pager->execute()->fetchAll();
      foreach($results as $res)
      {
        $email = $res->reciever_email;
        $message = $res->message;
        $user = $res->reciever;
      }
      $database = \Drupal::database();  
      $result = $database->select('users_field_data', 'u')
              ->fields('u')
              ->condition('uid',$user)
              ->execute()
              ->fetchAll();
    
      foreach($result as $ac)
      {
        $name = $ac->name.'('.$ac->uid.')';
        $uid = $ac->uid;
      }
      $form['open_form'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Wants to text mail.'),
        '#default_value' => 1
      ];
    }
    else
    {
      $form['open_form'] = [
        '#type'          => 'checkbox',
        '#title'         => $this->t('Wants to text mail.'),
      ];
    }
    
    $form['show_form']   = [
      '#type'            => 'container',
      '#attributes'      => [
        'class'          => 'show-form',
      ],
      '#states'          => [
                            'invisible' => [
                              'input[name="open_form"]' => [
                                'checked' => FALSE,
                              ],
                            ],
                          ],
    ];

    $form['show_form']['users_list']   = [
      '#type'                     => 'textfield',
      '#title'                    => $this->t('Search'),
      '#autocomplete_route_name'  => 'pingme.load_users_controller_LoadUsers',
      '#required'                 => false,
      '#default_value'            => $name != '' ? t($name) : '', 
    ]; 
    $form['show_form']['users_list1']   = [
      '#type'                     => 'hidden',
      '#autocomplete_route_name'  => 'pingme.load_users_controller_LoadUsers',
      '#required'                 => false,
      '#default_value'            => $uid != '' ? t($uid) : '', 
    ];
    
    $form['show_form']['message'] = [
      '#type'                     => 'textarea',
      '#title'                    => t('Message'),
      '#required'                 => true,
      '#resizable'                => FALSE,
      '#rows'                     => 3,
      '#default_value'            => $message!= '' ? t($message) : '',
    ];

    $form['actions']              = array('#type' => 'actions');
    $form['show_form']['actions']['send'] = [
      '#type'                     => 'submit',
      '#value'                    => $param != '' ? $this->t('Update') : $this->t('Submit'),
      '#ajax'                     => [
        'callback'                => [$this, 'submitModalFormAjax'],
        'event'                   => 'click',
      ],
      // '#attributes' => [
      //   'class' => [
      //     'use-ajax',
      //   ],
      // ],
    ];

    $form['text_footer'] = array
    (
      '#prefix' => '<hr>',
      '#suffix' => '</hr>',
    );
    $form['#attached']['library'] = [
      'core/drupal.dialog.ajax',
      'pingme/pingme.tree'
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) 
  {
    
    if($form_state->getValue('id') != '')
    {
      // UPDATE EXISTING RECORD VIA AJAX
      
      $id       = $form_state->getValue('id');
      $user_id  = $form_state->getValue('users_list1');
      $msg      = $form_state->getValue('message');
      
      $query    = \Drupal::database()->select('users_field_data', 'u');
      $query->fields('u', ['uid','name','mail']);
      $query->condition('uid',$user_id);
      $results  = $query->execute()->fetchAll();
      foreach($results as $row => $res)
      {
        $mail   = $res->mail;
        $name   = $res->name; 
      }
      $fields   = array('reciever' => $user_id, 'reciever_name' => $name, 'message' => $msg , 'reciever_email' => $mail,'isDeleted'=>0);
      $query    = \Drupal::database();
      $query->update('pingme')->fields($fields)->condition('id', $id)->execute();
      
      // END UPDATE FEATURE

      $text = "Record has been updated. Message Sent";
    }
    else
    {
      // INSERT NEW RECORD 
      $user_id  = $form_state->getValue('users_list');
      $msg      = $form_state->getValue('message');

      $query    = \Drupal::database()->select('users_field_data', 'u');
      $query->fields('u', ['uid','name','mail']);
      $query->condition('uid',$user_id);
      $results  = $query->execute()->fetchAll();
      foreach($results as $row => $res)
      {
        $mail = $res->mail;
        $name = $res->name; 
      }
      $fields = array('reciever' => $user_id, 'reciever_name' => $name, 'message' => $msg , 'reciever_email' => $mail,'isDeleted' => 0);
      $db     = \Drupal::database()->insert('pingme')
              ->fields(['reciever', 'reciever_name', 'message' , 'reciever_email', 'isDeleted'])
              ->values($fields)
              ->execute();
      // END INSERTION
      $text = "New Record Added. Message Sent";
    }
    // MAIL Feature
    // $mailManager       = \Drupal::service('plugin.manager.mail');
    // $module            = 'pingme';
    // $key               = 'general_mail';
    // $to                = $mail;
    // $params['message'] = $form_state->getValue('message');
    // $params['subject'] = "Mail subject";
    // $langcode          = \Drupal::currentUser()->getPreferredLangcode();
    // $send              = true;
    // $result = $mailManager->mail($module, $key, $to, $langcode, $params, NULL, $send);
    // END MAIL

    \Drupal::messenger()->addMessage(t($text.' : '.$name));
  }

  public function submitModalFormAjax(array $form, FormStateInterface $form_state, string $id = NULL) {
    $response = new AjaxResponse();

    // If there are any form errors, re-display the form.
    if ($form_state->hasAnyErrors()) {
      $response->addCommand(new ReplaceCommand('#modal_example_form', $form));
    }
    else {
      $response->addCommand(new OpenModalDialogCommand("Success!", 'Message Sent to the user.', ['width' => 800]));
    }
    return $response;
  }
  protected function getEditableConfigNames() {
    return ['config.pingme'];
  }
}