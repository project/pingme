<?php

namespace Drupal\pingme\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\ConfirmFormBase;
use Drupal\Core\Url;
use Drupal\Core\Render\Element;

/**
 * Class DeleteMessagesForm.
 */
class DeleteMessagesForm extends ConfirmFormBase {

  /**
   * ID of the item to delete.
   *
   * @var int
   */
  protected $id;

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'delete_messages_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state, string $id = NULL) {
    $this->id = $id;
    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    // Display result.
    $query = \Drupal::database();
    $query->delete('pingme')
                ->condition('id',$this->id)
               ->execute();
    \Drupal::messenger()->addMessage(t('Message Deleted.'), 'success');
    $form_state->setRedirect('pingme.pingme_data_controller_PingMeData');
  }

  /**
   * {@inheritdoc}
   */
  public function getCancelUrl() {
    return new Url('pingme.pingme_data_controller_PingMeData');
  }

  /**
   * {@inheritdoc}
   */
  public function getQuestion() {
    return $this->t('Do you want to delete %id?', ['%id' => $this->id]);
  }

}
