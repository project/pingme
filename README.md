# PingME

This module basically designed for new drupal members to learn about
customization of module development.

Features:
- Schema
- Curd Operations with Ajax.
- Modal Popup
- Record List
- Custom Drupal Mail Feature.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/pingme).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/pingme).


## Table of contents

- Requirements
- Installation
- Configuration
- Maintainers


## Requirements

This module requires no modules outside of Drupal core.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

**Path**: `/ping-me/records`

All operations can be done from mentioned path.


## Maintainers

- Deepak Bhati - [heni_deepak](https://www.drupal.org/u/heni_deepak)
- Radheshyam Kumawat - [radheymkumar](https://www.drupal.org/u/radheymkumar)